## Configure Firebase on project

### Create Firebase project on android

* Go to [Firebase console](https://console.firebase.google.com/u/0/) and click on "Add project" or open an existing project.
* Click on add firebase to your project
* Fill the form with `com.caboabanana` on "Android package name"
* Click on "Register app" and download `google-services.json` on `android/app/` folder.
* Click on the `X` to close this "modal".
* On the left side menu, click on "Authentication"
* Go to "Login methods" tab and enable the "Email/Password" option. (We don't use the Email link (without password) method, so you don't have to enable it).

Your project has been configured :D
