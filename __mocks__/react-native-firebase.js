const firebase = {
    messaging: jest.fn(() => ({
        hasPermission: jest.fn(() => Promise.resolve(true)),
        subscribeToTopic: jest.fn(),
        unsubscribeFromTopic: jest.fn(),
        requestPermission: jest.fn(() => Promise.resolve(true)),
        getToken: jest.fn(() => Promise.resolve('myMockToken')),
    })),
    notifications: jest.fn(() => ({
        onNotification: jest.fn(),
        onNotificationDisplayed: jest.fn(),
    })),
    auth: jest.fn(() => ({
        signInAndRetrieveDataWithCustomToken:
            jest.fn(() => Promise.resolve({ data: 'ok', status: 200 })),
        signInAndRetrieveDataWithEmailAndPassword:
            jest.fn(() => Promise.resolve({ data: 'ok', status: 200 })),
        signOut:
            jest.fn(() => Promise.resolve({ data: 'ok', status: 200 })),
        currentUser: {
            getIdToken:
                jest.fn(() => Promise.resolve({ data: 'ok', status: 200 })),
        },
    })),
};

export default firebase;
