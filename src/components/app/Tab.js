import * as React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import TabIcon from '../common/TabIcon';
import GroupsScreen from '../groups/GroupsScreen';
import HistoricoScreen from '../historico/HistoricoScreen';
import NotificationScreen from '../notification/NotificationScreen';

type Props = {
    tintColor: string,
};

export default createBottomTabNavigator({
    Groups: { screen: GroupsScreen },
    History: { screen: HistoricoScreen },
    Notification: { screen: NotificationScreen },
}, {
    initialRouteName: 'Groups',
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ tintColor }: Props) => {
            const { routeName } = navigation.state;
            let iconName;
            let typeName = '';
            if (routeName === 'Groups') {
                iconName = 'people';
            } else if (routeName === 'History') {
                iconName = 'history';
            } else {
                iconName = 'exclamation';
                typeName = 'font-awesome';
            }
            return (<TabIcon
              iconName={iconName}
              typeName={typeName}
              tintColor={tintColor}
            />);
        },
    }),
    tabBarPosition: 'bottom',
    tabBarOptions: {
        style: {
            borderTopColor: '#D2DAE6',
            borderTopWidth: 0.5,
            backgroundColor: 'white',
        },
        showLabel: false,
        activeTintColor: '#5A6978',
        inactiveTintColor: '#E5E9F2',
    },
    animationEnabled: true,
    swipeEnabled: false,
});
