/* @flow */

import React from 'react';
import { createStackNavigator } from 'react-navigation';
import SignUpScreen from '../signup/SignUpScreen';
import LoginScreen from '../login/LoginScreen';
import Toolbar from '../common/Toolbar';
import Tab from './Tab';
import GroupCreationScreen from '../groupcreation/GroupCreationScreen';
import GroupDetailsScreen from '../groupdetails/GroupDetailsScreen';

const App = createStackNavigator(
    {
        Login: LoginScreen,
        SignUp: SignUpScreen,
        LoggedScreens: Tab,
        GroupCreation: GroupCreationScreen,
        GroupDetails: GroupDetailsScreen,
    },
    {
        initialRouteName: 'Login',
        headerMode: <Toolbar />,
    },
);

export default App;
