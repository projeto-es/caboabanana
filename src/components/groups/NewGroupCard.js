import React from 'react';
import { TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';

type Props = {
    onPress: () => void,
};

const styles = StyleSheet.create({
    container: {
        height: (28 * Dimensions.get('window').height) / 100,
        width: (37 * Dimensions.get('window').width) / 100,
        backgroundColor: '#C0CCDA',
        borderTopRightRadius: 3,
        borderTopLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderBottomLeftRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        color: '#47525E',
        fontSize: 18,
        margin: 8,
        fontWeight: 'bold',
    },
});

const NewGroupCard = ({ onPress }: Props) => (
    <TouchableOpacity
      style={styles.container}
      onPress={onPress}
      activeOpacity={1}
    >
        <Icon
          name="add-circle-outline"
          color="#EFF2F7"
        />
        <Text style={styles.title} >
          Novo Grupo
        </Text>
    </TouchableOpacity>
);

export default NewGroupCard;
