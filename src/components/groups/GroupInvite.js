/* @flow */

import React from 'react';
import { View, Text } from 'react-native';
import { Formik } from 'formik';
import Modal from 'react-native-modal';
import * as yup from 'yup';
import { EmailValidator } from '../common/validation/UserValidation';
import styles from '../common/Styles';
import InputField from '../common/InputField';
import FormButton from '../common/FormButton';

type Values = {
    email: string,
};

type InviteInnerFormProps = {
    values: Values,
    errors: Values,
    setFieldValue: (string: string, any: any) => void,
    handleSubmit: () => void,
};

export const InviteInnerForm = ({
    values,
    errors,
    setFieldValue,
    handleSubmit,
}: InviteInnerFormProps) => {
    const subtitle = ` Digite abaixo o email do usuário 
que deseja convidar para o grupo`;
    return (
        <View style={styles.inviteForm}>
            <Text style={styles.title}>Convidar para grupo</Text>
            <Text style={styles.subtitle}>{subtitle}</Text>
            <InputField
              placeholder="Email"
              value={values.email}
              errors={errors.email}
              onChangeText={text => setFieldValue('email', text)}
            />
            <FormButton text="Convidar" onPress={handleSubmit} />
        </View>
    );
};

const getInviteProps = (initialValues, onSubmit) => ({
    initialValues,
    validationSchema: yup.object().shape({
        email: EmailValidator(),
    }),
    validateOnChange: false,
    onSubmit: values => onSubmit(values),
});

type InviteFormProps = {
    onSubmit: (email: string) => void,
    initialValues?: {
        email: string,
    },
}

export const InviteForm = ({ onSubmit, initialValues }: InviteFormProps) => (
    <Formik {...getInviteProps(initialValues, onSubmit)}>
        {props => (<InviteInnerForm {...props} />)}
    </Formik>
);

InviteForm.defaultProps = {
    initialValues: {
        email: '',
    },
};

type Props = {
    modalVisible: boolean,
    onClose: () => void,
};

const GroupInvite = ({ modalVisible, onClose }: Props) => (
    <View style={styles.modalContainer}>
        <Modal
          isVisible={modalVisible}
          style={styles.modalStyle}
          onBackButtonPress={onClose}
          onBackdropPress={onClose}
        >
            <View style={styles.insideModal}>
                <InviteForm onSubmit={console.log} />
            </View>
        </Modal>
    </View>
);

export default GroupInvite;
