/* @flow */

import React, { Component } from 'react';
import { View } from 'react-native';
import { Icon } from 'react-native-elements';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import axios from 'axios';

import firebase from 'react-native-firebase';
import Toolbar from '../common/Toolbar';
import GroupGrid from './GroupGrid';
import styles from '../common/Styles';
import BASE_URL from '../common/consts/baseUrl';

type Props = {
    navigation: NavigationScreenProp<NavigationState, *>,
};

type Group = {
    key: number,
    title: string,
    color: string,
    participants: Array<string>,
};

type State = {
    groups: Array<Group>,
}

export const parse = (res: { data: any[] }) => {
    const groups = res.data.map((group, index) => {
        const participants = group.members.map(member => member.name);
        const parsedGroup = {
            title: group.name,
            color: group.color,
            key: index + 1,
            participants,
        };
        return parsedGroup;
    });
    groups.push({
        title: '', color: '', participants: [], key: 0,
    });
    return groups;
};

class GroupsScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = { groups: [] };
    }

    async componentDidMount() {
        const user = firebase.auth().currentUser;
        let id = '';
        if (user) id = await user.getIdToken();
        const params = { headers: { Authorization: `Bearer ${id}` } };
        return axios.get(`${BASE_URL}groups/`, params).then((res) => {
            const groups = parse(res);
            this.setState({ groups });
        }).catch((err) => {
            console.log(err);
        });
    }

    signOut = async () => {
        try {
            await firebase.auth().signOut();
            this.props.navigation.navigate('Login');
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Toolbar
                      text="Grupos"
                      rightIcon={<Icon
                        name="log-out"
                        type="feather"
                        onPress={() => this.signOut()}
                      />}
                    />
                </View>
                <View style={styles.grid}>
                    <GroupGrid
                      groups={this.state.groups}
                      navigate={this.props.navigation.navigate}
                    />
                </View>
            </View>
        );
    }
}

export default GroupsScreen;
