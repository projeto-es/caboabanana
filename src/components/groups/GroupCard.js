/* @flow */

import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
} from 'react-native';
import NewGroupCard from './NewGroupCard';

type Props = {
    color?: string,
    title?: string,
    buyButton?: () => void,
    inviteButton?: () => void,
    onPress: () => void,
    newOnPress: () => void,
};

const createStyles = color => (
    StyleSheet.create({
        container: {
            height: (28 * Dimensions.get('window').height) / 100,
            width: (37 * Dimensions.get('window').width) / 100,
            justifyContent: 'center',
        },
        top: {
            flex: 6,
            backgroundColor: color,
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
        },
        bottom: {
            flex: 5,
            backgroundColor: '#EFF2F7',
            borderBottomRightRadius: 3,
            borderBottomLeftRadius: 3,
            justifyContent: 'space-between',
        },
        titleContainer: {
            marginTop: 3,
        },
        buttonContainer: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            margin: 8,
            marginBottom: 15,
        },
        title: {
            color: '#47525E',
            fontSize: 18,
            margin: 8,
            fontWeight: '600',
        },
        button: {
            color: '#47525E',
            fontSize: 7,
            marginRight: 5,
            fontWeight: '600',
            letterSpacing: -0.5,
        },
    })
);

const GroupCard = ({
    title,
    color,
    onPress,
    newOnPress,
    buyButton,
    inviteButton,
}: Props) => {
    if (
        title === ''
        && color === ''
    ) {
        return <NewGroupCard onPress={newOnPress} />;
    }
    const styles = createStyles(color);
    return (
        <TouchableOpacity
          style={styles.container}
          onPress={onPress}
          activeOpacity={1}
        >
            <View style={styles.top} />
            <View style={styles.bottom}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>
                        {title}
                    </Text>
                </View>
                <View style={styles.buttonContainer}>
                    <Text style={styles.button} onPress={buyButton} >
                        COMPRAR
                    </Text>
                    <Text style={styles.button} onPress={inviteButton} >
                        CONVIDAR
                    </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

GroupCard.defaultProps = {
    color: undefined,
    title: undefined,
    buyButton: () => undefined,
    inviteButton: () => undefined,
};

export default GroupCard;
