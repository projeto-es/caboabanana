/* @flow */

import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import GroupCard from './GroupCard';
import GroupInvite from './GroupInvite';

type Group = {
    key: number,
    title: string,
    color: string,
    participants: Array<any>,
};

type Props = {
    groups: Array<Group>;
    navigate: (screen: string, params?: any) => void,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    item: {
        margin: 25,
    },
});

type State = {
    modalVisible: boolean,
};

class GroupGrid extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        if (props.groups.length === 0 ||
            props.groups[props.groups.length - 1].key !== '0') {
            props.groups.push({
                title: '',
                color: '',
                key: 0,
                participants: [],
            });
        }
        this.state = { modalVisible: false };
        this.setModalVisible = this.setModalVisible.bind(this);
    }

    setModalVisible = (visibility: boolean) => {
        this.setState({ modalVisible: visibility });
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                  numColumns={2}
                  justifyContent="space-between"
                  data={this.props.groups}
                  renderItem={({ item }) => (
                      <View style={styles.item}>
                          <GroupCard
                            title={item.title}
                            color={item.color}
                            onPress={
                                () => this.props
                                    .navigate('GroupDetails', {
                                        group: item,
                                    })
                            }
                            newOnPress={
                                () => this.props.navigate('GroupCreation')
                            }
                            inviteButton={() => this.setModalVisible(true)}
                          />
                      </View>
                  )}
                />
                <GroupInvite
                  modalVisible={this.state.modalVisible}
                  onClose={() => this.setModalVisible(false)}
                />
            </View>);
    }
}

export default GroupGrid;
