import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
} from 'react-native';
import ColorCircle from '../common/ColorCircle';

type Props = {
    onPress: () => void,
};

const styles = StyleSheet.create({
    text: {
        marginLeft: 42,
        fontSize: 17,
        marginBottom: 15,
        color: '#8190A5',
    },
    color: {
        marginHorizontal: 4,
    },
});

const colors = [
    '#DDDDCD',
    '#F1E4BA',
    '#FBEC5D',
    '#EDD151',
    '#8AE429',
    '#4D7F17',
    '#A45E3C',
    '#6B3E26',
    '#66585B',
];

const keyExtractor = item => item;

const ColorPicker = ({ onPress } : Props) => (
    <View>
        <Text style={styles.text}>
          Escolha a cor do grupo:
        </Text>
        <View>
            <FlatList
              alignSelf="center"
              numColumns={colors.length}
              data={colors}
              keyExtractor={keyExtractor}
              renderItem={({ item }) => (
                  <View
                    style={styles.color}
                  >
                      <ColorCircle
                        color={item}
                        onPress={onPress}
                      />
                  </View>
                      )}
            />
        </View>
    </View>
);

export default ColorPicker;
