/* @flow */

import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import axios from 'axios';
import firebase from 'react-native-firebase';
import Toolbar from '../common/Toolbar';
import Logo from '../common/Logo';
import GroupCreationForm from './GroupCreationForm';
import styles from '../common/Styles';
import BASE_URL from '../common/consts/baseUrl';

const screenStyles = StyleSheet.create({
    logo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    form: {
        flex: 1,
    },
});

type Props = {
    navigation: NavigationScreenProp<NavigationState, *>,
};

type NewGroup = {
    color: string,
    title: string,
};

export const submit = async (
    values: NewGroup,
    navigate: (screen: string, params: any) => void,
) => {
    const user = firebase.auth().currentUser;
    let id = '';
    if (user) id = await user.getIdToken();
    const params = { headers: { Authorization: `Bearer ${id}` } };

    return axios.post(`${BASE_URL}groups/`, Object
        .assign({}, values, params))
        .then((res: any) => {
            navigate('GroupDetails', {
                group: res,
            });
        }).catch((err) => {
            console.log(err);
        });
};

const GroupCreationScreen = ({ navigation }: Props) => (
    <View style={styles.container}>
        <Toolbar
          text="Novo Grupo"
          leftIcon={
              <Icon
                name="ios-arrow-back"
                type="ionicon"
                onPress={() => navigation.goBack()}
              />
            }
        />
        <View style={screenStyles.logo}>
            <Logo height={150} />
        </View>
        <View style={screenStyles.form}>
            <GroupCreationForm
              onSubmit={values => submit(values, navigation.navigate)}
            />
        </View>
    </View>
);

export default GroupCreationScreen;
