/* @flow */

import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import ColorPicker from './ColorPicker';
import InputField from '../common/InputField';
import FormButton from '../common/FormButton';
import {
    NameValidator,
    ColorValidator,
} from '../common/validation/GroupValidation';

const styles = StyleSheet.create({
    input: {
        alignItems: 'center',
    },
    button: {
        paddingTop: 35,
    },
    colorError: {
        color: '#FF2D00',
        fontSize: 12,
        marginLeft: 59,
    },
});

type Values = {
    title: string,
    color: string,
}

type Props = {
    values: Values,
    errors: Values,
    setFieldValue: (string, any) => void,
    handleSubmit: () => void,
}

export const GroupCreationInnerForm = ({
    values, errors, setFieldValue, handleSubmit,
}: Props) => (
    <View>
        <View style={styles.input}>
            <InputField
              placeholder="Nome do Grupo"
              value={values.title}
              password={false}
              errors={errors.title}
              onChangeText={text => setFieldValue('title', text)}
            />
        </View>
        <ColorPicker onPress={color => setFieldValue('color', color)} />
        <Text style={styles.colorError}>{errors.color}</Text>
        <View style={styles.button}>
            <FormButton text="Criar grupo" onPress={handleSubmit} />
        </View>
    </View>
);

type FormProps = {
    onSubmit: (values: Values) => Promise<any>,
    initialValues?: Values,
}

const getGroupCreationFormProps = (initialValues, onSubmit) => ({
    initialValues,
    validationSchema: yup.object().shape({
        title: NameValidator(),
        color: ColorValidator(),
    }),
    validateOnChange: false,
    onSubmit: values => onSubmit(values),
});

const GroupCreationForm = ({ initialValues, onSubmit }: FormProps) => (
    <Formik {...getGroupCreationFormProps(initialValues, onSubmit)}>
        {props => <GroupCreationInnerForm {...props} />}
    </Formik>
);

GroupCreationForm.defaultProps = {
    initialValues: {
        title: '',
        color: '',
    },
};

export default GroupCreationForm;
