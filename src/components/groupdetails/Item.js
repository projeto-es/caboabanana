/* @flow */

import React from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import styles from '../common/Styles';

type Props = {
    name: string,
    addedBy: string,
    onDelete: () => void,
}

const Item = ({ name, addedBy, onDelete }: Props) => (
    <View style={styles.itemContainer}>
        <View style={styles.itemTextContainer}>
            <Text style={styles.itemName}>
                {name}
            </Text>
            <Text style={styles.itemAddedBy}>
                Adicionado por {addedBy}
            </Text>
        </View>
        <View style={styles.itemButtonContainer}>
            <Icon
              name="x"
              type="feather"
              color="#31353C"
              onPress={onDelete}
            />
        </View>
    </View>
);

export default Item;
