/* @flow */

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Toolbar from '../common/Toolbar';
import ColorCircle from '../common/ColorCircle';
import styles from '../common/Styles';
import FormButton from '../common/FormButton';
import GroupInvite from '../groups/GroupInvite';

type Props = {
    navigation: NavigationScreenProp<NavigationState, *>,
    addItemButton: () => void,
};

type Group = {
    key: number,
    title: string,
    color: string,
    participants: Array<string>,
};

type State = {
    modalVisible: boolean,
    group: Group,
};

class GroupDetailsScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            modalVisible: false,
            group: props.navigation.getParam('group', {
                key: 0,
                title: 'ERRO',
                color: '#000000',
                participants: ['Erro'],
            }),
        };
        this.inviteButton = this.inviteButton.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        this.startButton = this.startButton.bind(this);
        this.formatParticipantsString = this
            .formatParticipantsString.bind(this);
    }

    setModalVisible = (visibility: boolean) => {
        this.setState({ modalVisible: visibility });
    }

    formatParticipantsString = () => {
        let output = this.state.group.participants.join(', ');
        if (output.length > 46) {
            output = output.slice(0, 46);
            output += '...';
        }
        return output;
    };

    startButton = () => true;

    inviteButton = () => this.setModalVisible(true);

    render() {
        return (
            <View style={styles.container}>
                <Toolbar
                  text={this.state.group.title}
                  leftIcon={<Icon
                    size={35}
                    name="ios-arrow-back"
                    type="ionicon"
                    onPress={() => this.props.navigation.goBack()}
                  />}
                  rightIcon={<ColorCircle
                    color={this.state.group.color}
                    onPress={() => undefined}
                  />}
                />
                <Text style={styles.participants}>
                    {this.formatParticipantsString()}
                </Text>
                <Text
                  style={styles.inviteButtonGroupDetails}
                  onPress={() => this.inviteButton()}
                >
                    Convidar
                </Text>
                <Text
                  style={styles.startButtonGroupDetails}
                  onPress={() => this.startButton()}
                >
                    Iniciar Compra
                </Text>
                <View style={styles.addItemButtonContainer} >
                    <FormButton
                      onPress={this.props.addItemButton}
                      text="Adicionar Item"
                      isAddButton
                    />
                </View>
                <GroupInvite
                  modalVisible={this.state.modalVisible}
                  onClose={() => this.setModalVisible(false)}
                />
            </View>
        );
    }
}

export default GroupDetailsScreen;
