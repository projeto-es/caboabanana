import React from 'react';
import { View } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import InputField from '../common/InputField';
import FormButton from '../common/FormButton';
import styles from '../common/Styles';
import {
    EmailValidator,
    PasswordValidator,
} from '../common/validation/UserValidation';

type Values = {
    email: string,
    password: string,
};

type Props = {
    values: Values,
    errors: Values,
    setFieldValue: (string, any) => void,
    handleSubmit: () => void,
};

export const LoginInnerForm = ({
    values, errors, setFieldValue, handleSubmit,
}: Props) => (
    <View style={styles.formView}>
        <InputField
          placeholder="Email"
          value={values.email}
          errors={errors.email}
          onChangeText={text => setFieldValue('email', text)}
        />
        <InputField
          placeholder="Senha"
          value={values.password}
          password
          errors={errors.password}
          onChangeText={text => setFieldValue('password', text)}
        />
        <FormButton onPress={handleSubmit} text="Login" />
    </View>
);

type FormProps = {
    onSubmit: (values: Values) => void,
    initialValues: Values,
};

const getLoginFormProps = (initialValues, onSubmit) => ({
    initialValues,
    validationSchema: yup.object().shape({
        email: EmailValidator(),
        password: PasswordValidator(),
    }),
    validateOnChange: false,
    onSubmit: values => onSubmit(values),
});

const LoginForm = ({ onSubmit, initialValues }: FormProps) => (
    <Formik {...getLoginFormProps(initialValues, onSubmit)}>
        {props => <LoginInnerForm {...props} />}
    </Formik>
);

LoginForm.defaultProps = {
    initialValues: {
        email: '',
        password: '',
    },
};

export default LoginForm;
