/* @flow */

import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import firebase from 'react-native-firebase';
import LoginForm from './LoginForm';
import Logo from '../common/Logo';
import LoginLink from '../common/LoginLink';
import SnackBar from '../common/SnackBar';

type Props = {
    navigation: NavigationScreenProp<NavigationState, *>;
};

type State = {
    snackbarVisible: boolean,
    snackbarError: boolean,
    snackbarText: string,
};

const formScreen = StyleSheet.create({
    boxView: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        justifyContent: 'space-around',
    },
    centerLogo: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexend: {
        flex: 1,
        justifyContent: 'center',
    },
});

class LoginScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            snackbarVisible: false,
            snackbarError: false,
            snackbarText: '',
        };

        this.submit = this.submit.bind(this);
        this.showError = this.showError.bind(this);
    }

    submit: (values: any) => void;
    submit(values: any) {
        firebase.auth()
            .signInAndRetrieveDataWithEmailAndPassword(
                values.email,
                values.password,
            )
            .then(() => {
                this.setState({
                    snackbarVisible: true,
                    snackbarError: false,
                    snackbarText: 'Login realizado com sucesso',
                });
                setTimeout(
                    () => this.props.navigation.replace('LoggedScreens'),
                    2000,
                );
            })
            .catch((error) => {
                let text;
                if (error.code === 'auth/user-not-found') {
                    text = 'Não encontramos um usuário com este email';
                } else if (error.code === 'auth/wrong-password') {
                    text = 'A senha está incorreta';
                } else {
                    text = 'Este usuário foi desativado';
                }

                this.showError(text);
            });
    }

    showError: (text: string) => void
    showError(text: string) {
        this.setState({
            snackbarVisible: true,
            snackbarError: true,
            snackbarText: text,
        });
    }

    render() {
        const snackbar = this.state.snackbarVisible ?
            (<SnackBar
              text={this.state.snackbarText}
              error={this.state.snackbarError}
            />) : null;
        return (
            <View style={formScreen.boxView}>
                {snackbar}
                <View style={formScreen.centerLogo}>
                    <Logo height={150} />
                </View>
                <View>
                    <LoginForm
                      onSubmit={this.submit}
                    />
                </View>
                <View style={formScreen.flexend}>
                    <LoginLink
                      onPress={() => this.props.navigation.navigate('SignUp')}
                      mainText="Ainda não tem uma conta? "
                      linkText="Crie aqui"
                    />
                </View>
                <View>
                    <Button
                      buttonStyle={{ backgroundColor: 'orange' }}
                      title="Tela de Grupos"
                      onPress={() => this.props.navigation.navigate('Groups')}
                    />
                </View>
            </View>
        );
    }
}


export default LoginScreen;
