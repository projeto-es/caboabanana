/* @flow */

import React from 'react';
import { Text, StyleSheet } from 'react-native';

type Props = {
    text: string,
};

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
        fontSize: 36,
        color: '#47525E',
        fontWeight: '200',
    },
});

const Header = ({ text }: Props) => (
    <Text style={styles.text}>
        {text}
    </Text>);

export default Header;
