/* @flow */

import React, { Component } from 'react';
import { View } from 'react-native';
import { NavigationScreenProp, NavigationState } from 'react-navigation';
import axios from 'axios';
import firebase from 'react-native-firebase';
import SignUpForm from './SignUpForm';
import Logo from '../common/Logo';
import styles from '../common/Styles';
import BASE_URL from '../common/consts/baseUrl';
import LoginLink from '../common/LoginLink';
import SnackBar from '../common/SnackBar';

type Values = {
    name: string,
    email: string,
    password: string,
    confirmPassword: string,
};

type Props = {
    navigation: NavigationScreenProp<NavigationState, *>,
};

type State = {
    snackbarVisible: boolean,
    snackbarError: boolean,
    snackbarText: string,
};

class SignUpScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            snackbarVisible: false,
            snackbarError: false,
            snackbarText: '',
        };

        this.showError = this.showError.bind(this);
        this.submit = this.submit.bind(this);
        this.successfullLogin = this.successfullLogin.bind(this);
        this.getErrorMessage = this.getErrorMessage.bind(this);
    }

    getErrorMessage = (message: string) => {
        let mess;
        if (message === 'auth/invalid-email') {
            mess = 'O email inserido é inválido';
        } else if (message === 'auth/user-disabled') {
            mess = 'O usuário está desabilitado';
        } else if (message === 'auth/user-not-found') {
            mess = 'O email inserido não está cadastrado';
        } else if (message === 'auth/wrong-password') {
            mess = 'Email/Senha incorretos';
        } else if (message === 'An existing account with' +
            ' this email already exists.') {
            mess = 'Já existe uma conta com o email inserido.';
        } else {
            mess = 'Servidor indisponível';
        }
        return mess;
    }

    successfullLogin = () => {
        this.setState({
            snackbarVisible: true,
            snackbarError: false,
            snackbarText: 'Cadastro feito com sucesso.',
        });
    }

    submit = async (data: Values) => {
        const newUser = {
            name: data.name,
            email: data.email,
            password: data.password,
        };
        try {
            await axios.post(`${BASE_URL}users/`, newUser);
            try {
                await firebase.auth()
                    .signInAndRetrieveDataWithEmailAndPassword(
                        newUser.email,
                        newUser.password,
                    );
                this.successfullLogin();
                setTimeout(() => this.props.navigation
                    .replace('LoggedScreens'), 2000);
            } catch (err2) {
                this.showError(this.getErrorMessage(err2.code));
            }
        } catch (err) {
            this.showError(this.getErrorMessage(err.response.data.message));
        }
    };

    showError = (text: string) => {
        this.setState({
            snackbarError: true,
            snackbarVisible: true,
            snackbarText: text,
        });
    }

    render() {
        const snackBar = this.state.snackbarVisible ? (<SnackBar
          error={this.state.snackbarError}
          text={this.state.snackbarText}
        />) : null;
        return (
            <View style={styles.boxView}>
                <View>
                    {snackBar}
                </View>
                <View style={styles.centerLogo}>
                    <Logo height={150} />
                </View>
                <View>
                    <SignUpForm
                      onSubmit={this.submit}
                    />
                </View>
                <View style={styles.flexend}>
                    <LoginLink
                      linkText="Entre aqui"
                      mainText="Já tem uma conta? "
                      onPress={() => this.props.navigation.navigate('Login')}
                    />
                </View>
            </View>
        );
    }
}

export default SignUpScreen;
