/* @flow */

import React from 'react';
import { View } from 'react-native';
import { Formik } from 'formik';
import * as yup from 'yup';
import FormButton from '../common/FormButton';
import {
    NameValidator,
    EmailValidator,
    PasswordValidator,
    ConfirmPasswordValidator,
} from '../common/validation/UserValidation';
import styles from '../common/Styles';
import InputField from '../common/InputField';

type Values = {
    name: string,
    email: string,
    password: string,
    confirmPassword: string,
}

type Props = {
    values: Values,
    errors: Values,
    setFieldValue: (string: string, any: any) => void,
    handleSubmit: () => void,
}

export const SignUpInnerForm = ({
    values, errors, setFieldValue, handleSubmit,
}: Props) => (
    <View style={styles.formView}>
        <InputField
          placeholder="Nome"
          value={values.name}
          password={false}
          errors={errors.name}
          onChangeText={text => setFieldValue('name', text)}
        />
        <InputField
          placeholder="Email"
          value={values.email}
          password={false}
          errors={errors.email}
          onChangeText={text => setFieldValue('email', text)}
        />
        <InputField
          placeholder="Senha"
          value={values.password}
          password
          errors={errors.password}
          onChangeText={text => setFieldValue('password', text)}
        />
        <InputField
          placeholder="Repita a Senha"
          value={values.confirmPassword}
          password
          errors={errors.confirmPassword}
          onChangeText={text => setFieldValue('confirmPassword', text)}
        />
        <FormButton text="Cadastrar" onPress={handleSubmit} />
    </View>
);

type FormProps = {
    onSubmit: (values: Values) => Promise<any>,
    initialValues?: Values,
}

const getSignUpFormProps = (initialValues, onSubmit) => ({
    initialValues,
    validationSchema: yup.object().shape({
        name: NameValidator(),
        email: EmailValidator(),
        password: PasswordValidator(),
        confirmPassword: ConfirmPasswordValidator(yup.ref('password')),
    }),
    validateOnChange: false,
    onSubmit: values => onSubmit(values),
});

const SignUpForm = ({ onSubmit, initialValues }: FormProps) => (
    <Formik {...getSignUpFormProps(initialValues, onSubmit)}>
        {props => <SignUpInnerForm {...props} />}
    </Formik>
);

SignUpForm.defaultProps = {
    initialValues: {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
    },
};

export default SignUpForm;
