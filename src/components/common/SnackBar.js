import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

type Props = {
    text: string,
    error?: boolean,
};

const backgroundColor = (error) => {
    if (error) {
        return '#F9686B';
    }
    return '#5FE28D';
};

const createStyles = error => (
    StyleSheet.create({
        container: {
            height: 20,
            backgroundColor: backgroundColor(error),
            justifyContent: 'center',
        },
        text: {
            fontSize: 13,
            textAlign: 'center',
            color: 'white',
        },
    })
);

const SnackBar = ({ text, error }: Props) => {
    const styles = createStyles(error);
    return (
        <View style={styles.container}>
            <Text style={styles.text}>
                {text}
            </Text>
        </View>
    );
};

SnackBar.defaultProps = {
    error: false,
};

export default SnackBar;
