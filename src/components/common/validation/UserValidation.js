import * as yup from 'yup';

function equalTo(ref: any, msg: string) {
    return yup.mixed().test({
        name: 'equalTo',
        exclusive: false,
        message: msg,
        params: {
            reference: ref.path,
        },
        test(value: any) {
            return value === this.resolve(ref);
        },
    });
}

yup.addMethod(yup.string, 'equalTo', equalTo);

export const NameValidator = () => yup.string()
    .required('Você precisa inserir um nome')
    .min(3, 'Seu nome precisa ter mais de 3 caracteres')
    .max(255, 'Seu nome não pode ter mais de 255 caracteres');

export const EmailValidator = () => yup.string()
    .required('É necessário inserir seu email')
    .email('Você precisa inserir um email válido');

export const PasswordValidator = () => yup.string()
    .required('É necessário digitar sua senha')
    .min(6, 'Sua senha deve ter entre 6 e 30 caracteres')
    .max(30, 'Sua senha deve ter entre 6 e 30 caracteres');

export const ConfirmPasswordValidator = password => yup.string()
    .equalTo(password, 'As senhas devem ser iguais');
