import * as yup from 'yup';

export const NameValidator = () => yup.string()
    .required('Você precisa inserir um nome')
    .min(3, 'Seu nome precisa ter mais de 3 caracteres')
    .max(40, 'Seu nome não pode ter mais de 40 caracteres');

export const ColorValidator = () => yup.string()
    .required('Você precisa escolher uma cor');
