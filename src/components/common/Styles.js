import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    itemContainer: {
        marginTop: '2%',
        alignSelf: 'center',
        width: '95%',
        height: '10%',
        backgroundColor: '#EFF2F7',
        opacity: 0.8,
        borderRadius: 5,
        flexDirection: 'row',
    },
    itemTextContainer: {
        flexDirection: 'column',
        width: '90%',
        paddingLeft: '3%',
        justifyContent: 'space-evenly',
    },
    itemName: {
        color: '#31353C',
        fontSize: 22,
        fontWeight: '500',
    },
    itemAddedBy: {
        color: '#8492A6',
        fontSize: 18,
        fontWeight: '300',
    },
    itemButtonContainer: {
        alignSelf: 'center',
        width: '5%',
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    addItemButtonContainer: {
        justifyContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        height: '15%',
        width: '100%',
        backgroundColor: '#ffffff',
        borderTopLeftRadius: 13,
        borderTopRightRadius: 13,
    },
    grid: {
        flex: 1,
    },
    participants: {
        fontSize: 16.5,
        fontFamily: 'Lato',
        color: '#47525E',
        paddingLeft: '5%',
        paddingRight: '5%',
    },
    inviteButtonGroupDetails: {
        fontSize: 15,
        paddingTop: '2%',
        fontFamily: 'Lato',
        color: '#47525E',
        fontWeight: 'bold',
        textAlign: 'right',
        paddingRight: '8.5%',
    },
    startButtonGroupDetails: {
        fontSize: 25,
        paddingTop: '2%',
        fontFamily: 'Lato',
        color: '#47525E',
        fontWeight: 'bold',
        textAlign: 'right',
        paddingRight: '8.5%',
    },
    boxView: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        justifyContent: 'space-around',
    },
    title: {
        fontSize: 22,
        fontWeight: '500',
        alignSelf: 'center',
    },
    subtitle: {
        paddingTop: '3%',
        paddingBottom: '10%',
        fontSize: 14,
        alignSelf: 'center',
    },
    bold: {
        fontWeight: 'bold',
    },
    link: {
        justifyContent: 'flex-end',
        alignSelf: 'center',
    },
    centerLogo: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    flexend: {
        flex: 1,
        justifyContent: 'center',
    },
    formView: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    inviteForm: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    snackView: {
        minWidth: '100%',
    },
    modalStyle: {
        alignSelf: 'center',
        width: '80%',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    insideModal: {
        paddingTop: '5%',
        paddingBottom: '5%',
        borderRadius: 5,
        width: '100%',
        height: 250,
        backgroundColor: 'white',
    },
});

export default styles;
