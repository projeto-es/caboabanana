import React from 'react';
import { View } from 'react-native';
import { Input } from 'react-native-elements';

type Props = {
  placeholder: string,
  value: string,
  onChangeText: () => void,
  errors: string,
  password: boolean
};

const styles = {
    viewBox: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '85%',
        padding: 0,
    },
    inputContainer: {
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
        borderRadius: 6,
        borderWidth: 0.6,
        borderColor: '#666',
    },
    container: {
        padding: 0,
        height: 63,
    },
    error: {
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 10,
        margin: 0,
    },
};

const InputField = ({
    placeholder, value, errors, password, onChangeText,
}: Props) => (
    <View style={styles.viewBox}>
        <Input
          containerStyle={styles.container}
          inputContainerStyle={styles.inputContainer}
          placeholder={placeholder}
          value={value}
          onChangeText={onChangeText}
          errorMessage={errors}
          errorStyle={styles.error}
          secureTextEntry={password}
        />
    </View>
);

export default InputField;
