import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';

type Props = {
    text: string,
    onPress: () => void,
    isAddButton?: boolean,
};

const styles = StyleSheet.create({
    addButtonContainer: {
        width: '90%',
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        width: '80%',
    },
    addButton: {
        elevation: 0,
    },
    button: {
        backgroundColor: '#47525E',
        borderRadius: 100,
        height: 45,
    },
});


const FormButton = ({ onPress, text, isAddButton }: Props) =>
    (
        <Button
          onPress={onPress}
          title={text}
          containerStyle={isAddButton ?
            styles.addButtonContainer :
            [styles.addButtonContainer, styles.container]}
          buttonStyle={isAddButton ?
            [styles.button, styles.addButton] :
            styles.button}
        />
    );

FormButton.defaultProps = {
    isAddButton: false,
};

export default FormButton;
