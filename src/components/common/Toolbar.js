/* @flow */

import * as React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { Header } from 'react-native-elements';

const styles = StyleSheet.create({
    text: {
        color: 'black',
        fontFamily: 'Lato',
        fontSize: 40,
        fontWeight: '400',
    },
    outerContainer: {
        backgroundColor: 'white',
        height: (13.6 * Dimensions.get('window').height) / 100,
        borderBottomWidth: 0,
    },
    innerContainer: {
        paddingTop: '3%',
        paddingLeft: '3%',
        paddingRight: '5.5%',
    },
});

type Props = {
    text?: string,
    leftIcon?: React.Element<'Icon'>,
    rightIcon?: React.Element<any>,
};

const Toolbar = ({
    text, leftIcon, rightIcon,
} : Props) => (
    <Header
      leftComponent={leftIcon}
      centerComponent={{ text, style: styles.text }}
      rightComponent={rightIcon}
      placement="left"
      outerContainerStyles={styles.outerContainer}
      innerContainerStyles={styles.innerContainer}
    />
);

Toolbar.defaultProps = {
    text: undefined,
    leftIcon: undefined,
    rightIcon: undefined,
};

export default Toolbar;
