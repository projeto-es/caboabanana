/* @flow */

import React from 'react';
import { Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';

type Props = {
    color: string,
    onPress: (color: string) => void,
};

const ColorCircle = ({ color, onPress } : Props) => (
    <Icon
      name="circle"
      type="font-awesome"
      color={color}
      size={(8.5 * Dimensions.get('window').width) / 100}
      onPress={() => (onPress(color))}
      underlayColor="transparent"
    />
);

export default ColorCircle;
