import React from 'react';
import { Icon } from 'react-native-elements';

type IconProps = {
    iconName: string,
    typeName: string,
    tintColor: string,
};

const TabIcon = ({ iconName, typeName, tintColor }: IconProps) => (<Icon
  name={iconName}
  type={typeName}
  color={tintColor}
/>);

export default TabIcon;
