import React from 'react';
import { Text } from 'react-native';
import styles from './Styles';

type LinkProps = {
    onPress: () => void,
    mainText: string,
    linkText: string,
}

const LoginLink = ({ mainText, linkText, onPress }: LinkProps) => (
    <Text style={styles.link} onPress={onPress}>
        <Text>{mainText}</Text>
        <Text style={styles.bold}>{linkText}</Text>
    </Text>
);

export default LoginLink;
