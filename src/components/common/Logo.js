/* @flow */

import React from 'react';
import { StyleSheet, Image } from 'react-native';

const image = require('../../../assets/images/logo.png');

type Props = {
    height: number,
};

const createStyles = (h, w) => (
    StyleSheet.create({
        img: {
            height: h,
            width: w,
        },
    })
);

const Logo = ({ height }: Props) => {
    const width = height * 1.78;
    const styles = createStyles(height, width);
    return (
        <Image
          source={image}
          style={styles.img}
        />
    );
};
export default Logo;
