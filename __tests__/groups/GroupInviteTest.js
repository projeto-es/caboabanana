import React from 'react';
import { shallow } from 'enzyme';
import { Formik } from 'formik';
import GroupInvite,
{ InviteForm, InviteInnerForm } from '../../src/components/groups/GroupInvite';

describe('Group Invite Tests', () => {
    describe('Group Invite tests', () => {
        it('matches snapshot', () => {
            const tree = shallow(<GroupInvite />);
            expect(tree).toMatchSnapshot();
        });
    });
    describe('Inner Form Tests', () => {
        it('matches snapshot', () => {
            const tree = shallow(<InviteInnerForm
              values={{ email: '' }}
              errors={{ email: '' }}
            />);
            expect(tree).toMatchSnapshot();
        });
    });
    describe('Outer Form Tests', () => {
        const spy = jest.fn();
        it('matches snapshot', () => {
            const tree = shallow(<InviteForm
              onSubmit={spy}
            />);
            expect(tree).toMatchSnapshot();
        });
        it('accepts valid email', async () => {
            const email = { email: 'banana@banana.com' };
            const tree = shallow(<InviteForm
              onSubmit={spy}
              initialValues={email}
            />);
            const formik = tree.find(Formik).dive();
            const innerForm = formik.find(InviteInnerForm).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            innerForm.update();
            formik.update();
            tree.update();

            expect(innerForm).toMatchSnapshot();
            expect(spy).toHaveBeenCalled();
        });
    });
});
