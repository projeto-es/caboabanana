import React from 'react';
import { shallow } from 'enzyme';
import NewGroupCard from '../../src/components/groups/NewGroupCard';

describe('NewGroupCard tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<NewGroupCard
              groups={undefined}
            />);
            expect(wrapper).toMatchSnapshot();
        });
        it('Executes function when pressed', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<NewGroupCard
              onPress={pressed}
            />);
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });
});
