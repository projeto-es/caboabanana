import React from 'react';
import { shallow } from 'enzyme';
import GroupGrid from '../../src/components/groups/GroupGrid';

describe('GroupCard tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<GroupGrid
              groups={[]}
              navigation={() => undefined}
            />);
            expect(wrapper).toMatchSnapshot();
        });
    });
});
