import React from 'react';
import { shallow } from 'enzyme';
import GroupCard from '../../src/components/groups/GroupCard';

describe('GroupCard tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<GroupCard
              title="test"
              color="black"
              onPress={() => undefined}
              buyButton={() => undefined}
              inviteButton={() => undefined}
            />);
            expect(wrapper).toMatchSnapshot();
        });
        it('Executes function when COMPONENT is pressed', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<GroupCard
              title="test"
              color="black"
              onPress={pressed}
              buyButton={() => undefined}
              inviteButton={() => undefined}
            />);
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
        it('Executes function when COMPRAR is pressed', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<GroupCard
              title="test"
              color="black"
              onPress={() => undefined}
              buyButton={pressed}
              inviteButton={() => undefined}
            />);
            wrapper.find('Text').at(1).simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
        it('Executes function when CONVIDAR is pressed', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<GroupCard
              title="test"
              color="black"
              onPress={() => undefined}
              buyButton={() => undefined}
              inviteButton={pressed}
            />);
            wrapper.find('Text').at(2).simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });
});
