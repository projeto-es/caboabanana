import React from 'react';
import { shallow } from 'enzyme';
import axios from 'axios';
import GroupsScreen, { parse } from '../../src/components//groups/GroupsScreen';

jest.mock('axios');

describe('GroupsScreen', () => {
    const spy = { navigate: jest.fn() };
    describe('should', () => {
        const mockedValue = {
            data: [{
                color: '#FF0000',
                name: 'Family',
                members: [
                    { name: 'dad' },
                ],
            }],
        };
        afterEach(() => {
            spy.navigate.mockReset();
        });
        it('load groups correctly', async () => {
            axios.get.mockResolvedValue(mockedValue);
            const tree = shallow(<GroupsScreen
              navigation={spy}
            />);
            await tree.instance().componentDidMount();
            tree.update();
            expect(tree).toMatchSnapshot();
            expect(tree.state().groups).toEqual(parse(mockedValue));
        });
        it('have a working signout', async () => {
            const tree = shallow(<GroupsScreen
              navigation={spy}
            />).instance();
            await tree.signOut();
            expect(spy.navigate).toHaveBeenCalledWith('Login');
        });
    });
});
