import React from 'react';
import { shallow } from 'enzyme';
import FormButton from '../../src/components/common/FormButton';

describe('FormButton tests', () => {
    describe('FormButton tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<FormButton
              text="Test"
              onPress={() => undefined}
            />);
            expect(wrapper).toMatchSnapshot();
        });
        it('Executes function when pressed', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<FormButton
              text="test"
              onPress={pressed}
            />);
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });
});
