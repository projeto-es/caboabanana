import React from 'react';
import { shallow } from 'enzyme';
import ColorCircle from '../../src/components/common/ColorCircle';

describe('ColorCircle tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly in yellow', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<ColorCircle
              color="yellow"
              onPress={pressed}
            />);
            expect(wrapper).toMatchSnapshot();
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
        it('renders correctly in green', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<ColorCircle
              color="green"
              onPress={pressed}
            />);
            expect(wrapper).toMatchSnapshot();
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
        it('renders correctly with a hex value', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<ColorCircle
              color="#f89bff"
              onPress={pressed}
            />);
            expect(wrapper).toMatchSnapshot();
            wrapper.simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });
});
