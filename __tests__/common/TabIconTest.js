import React from 'react';
import { shallow } from 'enzyme';
import TabIcon from '../../src/components/common/TabIcon';

describe('Tabs should render', () => {
    it('properly', async () => {
        const tree = shallow(<TabIcon />);
        expect(tree).toMatchSnapshot();
    });
    it('with orange color', () => {
        const tree = shallow(<TabIcon
          tintColor="orange"
        />);
        expect(tree.find({ color: 'orange' }).length).toBe(1);
    });
});
