import React from 'react';
import { shallow } from 'enzyme';
import InputField from '../../src/components/common/InputField';

describe('InputField tests', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<InputField
          placeholder="Teste"
          value=""
          password={false}
        />);
        expect(wrapper).toMatchSnapshot();
    });
    it('renders correctly with password', () => {
        const wrapper = shallow(<InputField
          placeholder="Teste"
          value=""
          password
        />);
        expect(wrapper).toMatchSnapshot();
    });
    it('renders correctly with error message', () => {
        const wrapper = shallow(<InputField
          placeholder="Teste"
          value=""
          errors="Uma mensagem linda aqui"
        />);
        expect(wrapper).toMatchSnapshot();
    });
});
