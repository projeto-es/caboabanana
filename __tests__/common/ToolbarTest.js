import React from 'react';
import { Icon } from 'react-native-elements';
import { shallow } from 'enzyme';
import Toolbar from '../../src/components/common/Toolbar';

describe('Toolbar tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<Toolbar
              text="Toolbar"
              leftIcon={<Icon name="arrow-back" />}
            />);
            expect(wrapper).toMatchSnapshot();
        });
    });
    describe('Icon should', () => {
        it('do something when pressed', () => {
            const spy = jest.fn();
            const wrapper = shallow(<Toolbar
              text="Toolbar"
              leftIcon={<Icon name="arrow-back" onPress={spy} />}
            />);
            wrapper.find('Header').props().leftComponent.props.onPress();
            expect(spy).toHaveBeenCalled();
        });
    });
});
