import React from 'react';
import { shallow } from 'enzyme';
import SnackBar from '../../src/components/common/SnackBar';

describe('SnackBar tests', () => {
    it('renders correctly in green', () => {
        const tree = shallow(<SnackBar text="" />);
        expect(tree).toMatchSnapshot();
    });
    it('renders correctly in red', () => {
        const tree = shallow(<SnackBar text="" error />);
        expect(tree).toMatchSnapshot();
    });
});
