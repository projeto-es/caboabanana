import React from 'react';
import { shallow } from 'enzyme';
import GroupDetailsScreen
    from '../../src/components/groupdetails/GroupDetailsScreen';

describe('GroupDetailsScreen tests', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });
    describe('Snapshot tests', () => {
        const spy = {
            navigate: jest.fn(),
            getParam: jest.fn(() => ({
                title: 'teste',
                color: 'teste',
                participants: ['testes', 'testes', 'testes'],
                key: 0,
            })),
        };
        it('renders correctly', () => {
            const wrapper = shallow(<GroupDetailsScreen
              navigation={spy}
            />);
            expect(wrapper).toMatchSnapshot();
        });
        it('Executes function when invite button is pressed', () => {
            const wrapper = shallow(<GroupDetailsScreen
              navigation={spy}
            />);
            const pressed = jest.spyOn(wrapper
                .instance(), 'inviteButton');
            wrapper.find('Text').at(1).simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
        it('Executes function when start button is pressed', () => {
            const tree = shallow(<GroupDetailsScreen
              navigation={spy}
            />);
            const pressed = jest.spyOn(tree
                .instance(), 'startButton');
            tree.find('Text').at(2).simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });

    describe('formatParticipantsString tests', () => {
        const spy = {
            navigate: jest.fn(),
        };
        it('works with 46+', () => {
            spy.getParam = jest.fn(() => ({
                participants: ['Eu',
                    'Você',
                    'Zubumafu',
                    'Torcida do Flamengo',
                    'Rohitt'],
            }));
            const { formatParticipantsString } = shallow(<GroupDetailsScreen
              navigation={spy}
            />).instance();
            expect(formatParticipantsString())
                .toEqual('Eu, Você, Zubumafu, Torcida do Flamengo, Rohit...');
        });
        it('works with another case of 46+', () => {
            spy.getParam = jest.fn(() => ({
                participants: ['Eu',
                    'Você',
                    'Zubumafu',
                    'Torcida do Flamengo',
                    'Rohit',
                    'Vitinho'],
            }));
            const { formatParticipantsString } = shallow(<GroupDetailsScreen
              navigation={spy}
            />).instance();
            expect(formatParticipantsString())
                .toEqual('Eu, Você, Zubumafu, Torcida do Flamengo, Rohit...');
        });
        it('works with case of 46-', () => {
            spy.getParam = jest.fn(() => ({
                participants: ['Eu',
                    'Você',
                    'Zubumafu',
                    'Torcida do Flamengo',
                    'Rohit'],
            }));
            const { formatParticipantsString } = shallow(<GroupDetailsScreen
              navigation={spy}
            />).instance();
            expect(formatParticipantsString())
                .toEqual('Eu, Você, Zubumafu, Torcida do Flamengo, Rohit');
        });
        it('works with minimal case', () => {
            spy.getParam = jest.fn(() => ({
                participants: ['Eu'],
            }));
            const { formatParticipantsString } = shallow(<GroupDetailsScreen
              navigation={spy}
            />).instance();
            expect(formatParticipantsString())
                .toEqual('Eu');
        });
    });

    describe('inviteModal tests', () => {
        const spy = {
            navigate: jest.fn(),
            getParam: jest.fn(() => ({
                participants: ['Eu',
                    'Você',
                    'Zubumafu',
                    'Torcida do Flamengo',
                    'Rohitt'],
            })),
        };
        const tree = shallow(<GroupDetailsScreen navigation={spy} />);
        afterEach(() => {
            tree.instance().setModalVisible(false);
        });
        it('modal opening works', () => {
            expect(tree.instance().state.modalVisible).not.toBeTruthy();
            tree.find('Text').at(1).simulate('press');
            expect(tree.instance().state.modalVisible).toBeTruthy();
        });
        it('modal closing works', () => {
            tree.find('Text').at(1).simulate('press');
            expect(tree.instance().state.modalVisible).toBeTruthy();
            tree.find('GroupInvite').props().onClose();
            expect(tree.instance().state.modalVisible).not.toBeTruthy();
        });
        it('modal snapshot', () => {
            expect(tree).toMatchSnapshot();
        });
    });
});
