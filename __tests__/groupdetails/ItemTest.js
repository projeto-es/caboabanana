import React from 'react';
import { shallow } from 'enzyme';
import Item from '../../src/components/groupdetails/Item';

describe('Item tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<Item
              name="NOME DO ITEM"
              addedBy="NOME DA PESSOA"
              onDelete={() => undefined}
            />);
            expect(wrapper).toMatchSnapshot();
        });
    });
    describe('Button test', () => {
        it('works correctly', () => {
            const pressed = jest.fn();
            const wrapper = shallow(<Item
              name="NOME DO ITEM"
              addedBy="NOME DA PESSOA"
              onDelete={pressed}
            />);
            wrapper.find('Icon').simulate('press');
            expect(pressed).toHaveBeenCalledTimes(1);
        });
    });
});

