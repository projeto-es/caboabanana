import React from 'react';
import { shallow } from 'enzyme';
import { Formik } from 'formik';
import LoginForm,
{ LoginInnerForm } from '../../src/components/login/LoginForm';

describe('Login Form tests', () => {
    const allInvalid = {
        email: 'ex',
        password: 'aaaaa',
    };
    const invalidValid = {
        email: 'ex',
        password: 'minhasenha',
    };
    const validInvalid = {
        email: 'exemplo@exemplo.com',
        password: 'aaaaa',
    };
    const allValid = { // All Valid
        email: 'exemplo@exemplo.com',
        password: 'minhasenha',
    };
    const emptyInvalid = { // Empty email and invalid password
        email: '',
        password: 'aaaaa',
    };
    const invalidEmpty = { // Empty password and invalid email
        email: 'ex',
        password: '',
    };
    const emptyEmpty = { // Both Empty
        email: '',
        password: '',
    };

    describe('should render', () => {
        it('without initial errors', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={emptyEmpty}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            // await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submit invalidEmpty', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={invalidEmpty}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submit emptyInvalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={emptyInvalid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('without errors after submit allValid', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={allValid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submit validInvalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={validInvalid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submit invalidValid', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={invalidValid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submit allInvalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<LoginForm
              initialValues={allInvalid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(LoginInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
    });
});
