import React from 'react';
import { shallow } from 'enzyme';
import firebase from 'react-native-firebase';
import LoginScreen from '../../src/components/login/LoginScreen';

const user = { email: 'banana@banana.com', password: 'banana' };

describe('Login Screen', () => {
    it('should render ok', () => {
        const tree = shallow(<LoginScreen />);
        expect(tree).toMatchSnapshot();
    });
    it('should have a navigation at the bottom', () => {
        const spy = { navigate: jest.fn() };
        const tree = shallow(<LoginScreen navigation={spy} />);
        tree.find('LoginLink').simulate('press');
        expect(spy.navigate).toHaveBeenCalled();
    });
    it('should render a LoginForm', () => {
        const tree = shallow(<LoginScreen />);
        expect(tree.find('LoginForm').length).toBe(1);
    });
    it('should show user not found error', async () => {
        firebase.auth.mockReturnValueOnce({
            signInAndRetrieveDataWithEmailAndPassword:
                jest.fn()
                    .mockRejectedValue({ code: 'auth/user-not-found' }),
        });
        const tree = shallow(<LoginScreen />);
        await tree.instance().submit(user);
        expect(tree.instance().state).toEqual({
            snackbarVisible: true,
            snackbarError: true,
            snackbarText: 'Não encontramos um usuário com este email',
        });
    });
    it('should show wrong password error', async () => {
        firebase.auth.mockReturnValueOnce({
            signInAndRetrieveDataWithEmailAndPassword:
                jest.fn()
                    .mockRejectedValue({ code: 'auth/wrong-password' }),
        });
        const tree = shallow(<LoginScreen />);
        await tree.instance().submit(user);
        expect(tree.instance().state).toEqual({
            snackbarVisible: true,
            snackbarError: true,
            snackbarText: 'A senha está incorreta',
        });
    });
    it('should show disabled user error', async () => {
        firebase.auth.mockReturnValueOnce({
            signInAndRetrieveDataWithEmailAndPassword:
                jest.fn()
                    .mockRejectedValue({ code: 'auth/user-disabled' }),
        });
        const tree = shallow(<LoginScreen />);
        await tree.instance().submit(user);
        expect(tree.instance().state).toEqual({
            snackbarVisible: true,
            snackbarError: true,
            snackbarText: 'Este usuário foi desativado',
        });
    });
    it('should show login success', async () => {
        firebase.auth.mockReturnValueOnce({
            signInAndRetrieveDataWithEmailAndPassword:
                jest.fn()
                    .mockResolvedValue({ user: { uid: 1337 } }),
        });
        const spy = { replace: jest.fn() };
        const tree = shallow(<LoginScreen navigation={spy} />);
        await tree.instance().submit(user);
        jest.runAllTimers();
        expect(tree.instance().state).toEqual({
            snackbarVisible: true,
            snackbarError: false,
            snackbarText: 'Login realizado com sucesso',
        });
    });
});
