import React from 'react';
import { shallow } from 'enzyme';
import axios from 'axios';
import firebase from 'react-native-firebase';
import SignUpScreen from '../../src/components/signup/SignUpScreen';

jest.mock('axios');

describe('SignUpScreen tests', () => {
    describe('Snapshot tests', () => {
        const navigation = { navigate: jest.fn() };
        it('renders correctly', () => {
            const wrapper = shallow(<SignUpScreen navigation={navigation} />);
            expect(wrapper).toMatchSnapshot();
        });
        it('renders form', () => {
            const tree = shallow(<SignUpScreen navigation={navigation} />);
            expect(tree.find('SignUpForm').length).toBe(1);
        });
    });
    describe('The link on the bottom of the screen should', () => {
        it('navigate to the login screen', () => {
            const navigation = { navigate: jest.fn() };
            const wrapper = shallow(<SignUpScreen navigation={navigation} />);
            wrapper.find('LoginLink').simulate('press');
            expect(navigation.navigate).toHaveBeenCalledWith('Login');
            navigation.navigate.mockReset();
            navigation.navigate.mockRestore();
        });
    });
    describe('Submission on SignUp', () => {
        const validAll = {
            name: 'foo bar',
            email: 'foo@bar.com',
            password: 'foobar',
            confirmPassword: 'foobar',
        };
        const SCREEN = 'LoggedScreens';
        const spy = { navigate: jest.fn(), replace: jest.fn() };
        afterEach(() => {
            spy.navigate.mockReset();
            spy.replace.mockReset();
        });
        describe('should', () => {
            afterEach(() => {
                spy.navigate.mockReset();
                spy.replace.mockReset();
            });
            it('make super submit work', async () => {
                const tree = shallow(<SignUpScreen
                  navigation={spy}
                />).instance();

                axios.post.mockResolvedValue({
                    data: {
                        name: validAll.name,
                        email: validAll.email,
                        uid: 'blablabla',
                    },
                    status: 201,
                });
                await tree.submit(validAll);
                jest.runAllTimers();
                expect(spy.replace).toHaveBeenCalledWith(SCREEN);
            });
            it('show green snackbar', async () => {
                const tree = shallow(<SignUpScreen
                  navigation={spy}
                />).instance();

                axios.post.mockResolvedValue({
                    data: {
                        name: validAll.name,
                        email: validAll.email,
                        uid: 'blablabla',
                    },
                    status: 201,
                });
                await tree.submit(validAll);
                jest.runAllTimers();
                expect(spy.replace).toHaveBeenCalledWith(SCREEN);
                expect(tree.state.snackbarVisible).toEqual(true);
                expect(tree.state.snackbarError).toEqual(false);
                expect(tree.state.snackbarText)
                    .toEqual('Cadastro feito com sucesso.');
            });
            describe('return error', () => {
                afterEach(() => {
                    spy.navigate.mockReset();
                });
                describe('from backend', () => {
                    afterEach(() => {
                        spy.navigate.mockReset();
                    });
                    it('when email already registered', async () => {
                        const tree = shallow(<SignUpScreen
                          navigation={spy}
                        />).instance();
                        axios.post.mockRejectedValue({
                            response: {
                                data: {
                                    message: 'An existing account with' +
                                    ' this email already exists.',
                                },
                            },
                        });
                        await tree.submit(validAll);
                        expect(spy.navigate).not.toHaveBeenCalled();
                    });
                    it('when unkown error in caboabanana server', async () => {
                        const tree = shallow(<SignUpScreen
                          navigation={spy}
                        />).instance();
                        axios.post.mockRejectedValue({
                            response: {
                                data: {
                                    message: 'unkown error',
                                },
                            },
                        });
                        await tree.submit(validAll);
                        expect(spy.navigate).not.toHaveBeenCalled();
                    });
                });
                describe('from firebase', () => {
                    it('when password is incorrect', async () => {
                        firebase.auth
                            .mockReturnValueOnce({
                                signInAndRetrieveDataWithEmailAndPassword:
                                jest.fn().mockRejectedValue({
                                    code: 'auth/wrong-password',
                                }),
                            });
                        const tree = shallow(<SignUpScreen
                          navigation={spy}
                        />).instance();
                        axios.post.mockResolvedValue({
                            data: {
                                name: validAll.name,
                                email: validAll.email,
                                uid: 'blablabla',
                            },
                            status: 201,
                        });
                        await tree.submit(validAll);
                        expect(spy.navigate).not.toHaveBeenCalled();
                    });
                    it('when account is disabled', async () => {
                        firebase.auth
                            .mockReturnValueOnce({
                                signInAndRetrieveDataWithEmailAndPassword:
                                jest.fn().mockRejectedValue({
                                    code: 'auth/user-disabled',
                                }),
                            });
                        const tree = shallow(<SignUpScreen
                          navigation={spy}
                        />).instance();
                        axios.post.mockResolvedValue({
                            data: {
                                name: validAll.name,
                                email: validAll.email,
                                uid: 'blablabla',
                            },
                            status: 201,
                        });
                        await tree.submit(validAll);
                        expect(spy.navigate).not.toHaveBeenCalled();
                    });
                });
            });
        });
    });
});
