import React from 'react';
import { shallow } from 'enzyme';
import { Formik } from 'formik';
import SignUpForm, { SignUpInnerForm }
    from '../../src/components/signup/SignUpForm';

describe('SignUpForm tests', async () => {
    describe('on submit form with', async () => {
        const validAll = {
            name: 'foo bar',
            email: 'foo@bar.com',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidName = {
            name: 'fa',
            email: 'foocom@foo.com',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidEmail1 = {
            name: 'faye',
            email: 'foocom@foo.',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidEmail2 = {
            name: 'faye',
            email: 'foocom@.com',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidEmail3 = {
            name: 'faye',
            email: '@foo.com',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidEmail4 = {
            name: 'faye',
            email: 'foocom@.',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidEmail5 = {
            name: 'faye',
            email: '@.',
            password: 'foobar',
            confirmPassword: 'foobar',
        };

        const invalidPassword = {
            name: 'faye',
            email: 'foocom@foo.com',
            password: 'foo',
            confirmPassword: 'foo',
        };

        const invalidConfirmPassword = {
            name: 'faye',
            email: 'foocom@foo.com',
            password: 'foobar',
            confirmPassword: 'foo',
        };

        const invalidAll = {
            name: 'fa',
            email: 'foo@foo.',
            password: 'fooba',
            confirmPassword: 'foo',
        };

        const emptyAll = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
        };

        describe('valid values should', async () => {
            it('call onSubmit function and do not show errors', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={validAll}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).toHaveBeenCalled();
                expect(spy).toHaveBeenCalledWith(validAll);
                expect(innerForm).toMatchSnapshot();
            });
            it('not call onSubmit and do not show errors', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={validAll}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                // await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
        });

        describe('invalid values should', async () => {
            it('not call onSubmit function and not show errors', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidAll}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it('call onSubmit function and show errors', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidAll}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
        });
        describe('show error when invalid email if email like', async () => {
            it('foocom@foo.', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidEmail1}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it('foocom@.com', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidEmail2}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it('@foo.com', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidEmail3}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it('foocom@.', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidEmail4}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it('@.', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  initialValues={invalidEmail5}
                  onSubmit={spy}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
        });
        it('show error when name is invalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<SignUpForm
              initialValues={invalidName}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(SignUpInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('show error when confirmPassword is invalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<SignUpForm
              initialValues={invalidConfirmPassword}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(SignUpInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('show error when password is invalid', async () => {
            const spy = jest.fn();
            const tree = shallow(<SignUpForm
              initialValues={invalidPassword}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(SignUpInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        describe('no values should', async () => {
            it('press button, not call function and show errors', async () => {
                const spy = jest.fn();
                const tree = shallow(<SignUpForm
                  onSubmit={spy}
                  initialValues={emptyAll}
                />);

                const formik = tree.find(Formik).dive();

                await formik.props().submitForm();
                await new Promise(r => setImmediate(r));

                formik.update();
                const innerForm = formik.find(SignUpInnerForm).dive();

                expect(spy).not.toHaveBeenCalled();
                expect(innerForm).toMatchSnapshot();
            });
            it(
                'do not press button, not call onSubmit and do not show errors',
                async () => {
                    const spy = jest.fn();
                    const tree = shallow(<SignUpForm
                      onSubmit={spy}
                      initialValues={emptyAll}
                    />);

                    const formik = tree.find(Formik).dive();

                    // await formik.props().submitForm();
                    await new Promise(r => setImmediate(r));

                    formik.update();
                    const innerForm = formik.find(SignUpInnerForm).dive();

                    expect(spy).not.toHaveBeenCalled();
                    expect(innerForm).toMatchSnapshot();
                },
            );
        });
    });
});
