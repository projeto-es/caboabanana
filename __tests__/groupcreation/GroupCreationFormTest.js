import React from 'react';
import { shallow } from 'enzyme';
import { Formik } from 'formik';
import GroupCreationForm, { GroupCreationInnerForm }
    from '../../src/components/groupcreation/GroupCreationForm';

describe('GroupCreationForm tests', () => {
    const valid = {
        title: 'banana',
        color: '#F1E4BA',
    };
    const invalidName = {
        title: 'ba',
        color: '#F1E4BA',
    };
    const emptyFields = {
        title: '',
        color: '',
    };
    describe('should render', () => {
        it('without initial errors', () => {
            const spy = jest.fn();
            const tree = shallow(<GroupCreationForm
              initialValues={emptyFields}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();
            formik.update();
            const innerForm = formik.find(GroupCreationInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submitting with both fields empty', async () => {
            const spy = jest.fn();
            const tree = shallow(<GroupCreationForm
              initialValues={emptyFields}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(GroupCreationInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with errors after submitting an invalid name', async () => {
            const spy = jest.fn();
            const tree = shallow(<GroupCreationForm
              initialValues={invalidName}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(GroupCreationInnerForm).dive();

            expect(spy).not.toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
        it('with no errors after submitting a valid entry', async () => {
            const spy = jest.fn();
            const tree = shallow(<GroupCreationForm
              initialValues={valid}
              onSubmit={spy}
            />);

            const formik = tree.find(Formik).dive();

            await formik.props().submitForm();
            await new Promise(r => setImmediate(r));

            formik.update();
            const innerForm = formik.find(GroupCreationInnerForm).dive();

            expect(spy).toHaveBeenCalled();
            expect(innerForm).toMatchSnapshot();
        });
    });
});
