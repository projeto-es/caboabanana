import React from 'react';
import { shallow } from 'enzyme';
import ColorPicker from '../../src/components/groupcreation/ColorPicker';

describe('ColorPicker tests', () => {
    describe('Snapshot tests', () => {
        it('renders correctly', () => {
            const wrapper = shallow(<ColorPicker onPress={() => undefined} />);
            expect(wrapper).toMatchSnapshot();
        });
    });
});
