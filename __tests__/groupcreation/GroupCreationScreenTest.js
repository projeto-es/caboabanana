import React from 'react';
import { shallow } from 'enzyme';
import axios from 'axios';
import GroupCreationScreen, { submit }
    from '../../src/components/groupcreation/GroupCreationScreen';

jest.mock('axios');

describe.only('GroupCreationScreen tests', () => {
    it('renders correctly', () => {
        const tree = shallow(<GroupCreationScreen navigation={jest.fn()} />);
        expect(tree).toMatchSnapshot();
    });
    it('has a functional back button', () => {
        const spy = { goBack: jest.fn() };
        const tree = shallow(<GroupCreationScreen navigation={spy} />);
        tree.dive()
            .find('Toolbar').dive().dive()
            .find('Icon')
            .simulate('press');
        expect(spy.goBack).toHaveBeenCalled();
    });
    describe('should have a', () => {
        it('working button', async () => {
            const spy = { navigate: jest.fn() };
            const response = {
                uid: 1,
                color: 'blue',
                title: 'lista1',
                author: {
                    uid: 2,
                    name: 'banana',
                    email: 'banana@banana.com',
                },
                members: [
                    {
                        uid: 2,
                        name: 'banana',
                        email: 'banana@banana.com',
                    },
                    {
                        uid: 3,
                        name: 'apple',
                        email: 'apple@apple.com',
                    },
                ],
            };
            axios.post.mockResolvedValue(response);
            await submit({ title: 'lista1', color: '#0000FF' }, spy.navigate);
            expect(spy.navigate).toHaveBeenCalled();
        });
    });
});
